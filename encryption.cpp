#include "encryption.h"
#include <fstream>

Encryption::~Encryption() {
    clear();
}

void Encryption::clear() {
    this->lines = 0;
    encrypted.clear();
    decrypted.clear();
}

Vector Encryption::get_decrypted_content() {
    return this->decrypted;
}

Vector Encryption::get_encrypted_content() {
    return this->encrypted;
}

void Encryption::read_decrypted_file(std::string in_path) {
    Vector vector;
    std::string line;
    std::ifstream in_file(in_path);
    while (!in_file.eof()) {
        getline(in_file, line);
        vector.push_back(line);
    }
    in_file.close();
    lines = vector.size();
    decrypted.from(vector);
    encrypt_content();
}

void Encryption::read_encrypted_file(std::string in_path) {
    Vector vector;
    std::string line;
    std::ifstream in_file(in_path);
    while (!in_file.eof()) {
        getline(in_file, line);
        vector.push_back(line);
    }
    in_file.close();
    lines = vector.size();
    encrypted.from(vector);
    decrypt_content();
}

void Encryption::write_decrypted_content(std::string out_path) {
    std::ofstream out_file(out_path);
    for (unsigned int i = 0; i < lines; i++) {
        out_file << this->decrypted.get(i) << (i + 1 == lines ? "" : "\n");
    }
    out_file.close();
}

void Encryption::write_encrypted_content(std::string out_path) {
    std::ofstream out_file(out_path);
    for (unsigned int i = 0; i < lines; i++) {
        out_file << this->encrypted.get(i) << (i + 1 == lines ? "" : "\n");
    }
    out_file.close();
}
