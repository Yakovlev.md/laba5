#ifndef LIST_H
#define LIST_H

#include <iostream>


class Vector {
    std::string *arr;
    unsigned int current_size;
public:
    Vector(unsigned int size = 0);
    ~Vector();

    
    void init(unsigned int size);
    
    void from(Vector& vector);

    std::string get(unsigned int index);

    void set(unsigned int index, std::string value);

    void push_back(std::string value);

    unsigned int size();

    void clear();

    friend std::ostream& operator<<(std::ostream&, Vector&);
};
#endif
