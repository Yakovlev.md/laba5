#include "vector.h"

Vector::Vector(unsigned int size) {
    this->init(size);
}

Vector::~Vector() {
    delete arr;
    this->current_size = 0;
}

void Vector::init(unsigned int size) {
    this->arr = new std::string[size];
    this->current_size = size;
}

void Vector::from(Vector& vector) {
    this->clear();
    this->init(vector.size());
    for (unsigned int i = 0; i < vector.size(); i++) {
        this->set(i, vector.get(i));
    }
}

std::string Vector::get(unsigned int index) {
    if (index >= this->size())
        throw std::out_of_range("Incorrect element index.");

    return this->arr[index];
}

void Vector::set(unsigned int index, std::string value) {
    if (index >= this->size())
        throw std::out_of_range("Incorrect element index.");

    this->arr[index] = value;
}

void Vector::push_back(std::string value) {
    std::string* buffer = this->arr;
    delete arr;
    this->init(current_size + 1);

    for (unsigned int i = 0; i < this->size() - 1; i++) {
        this->set(i, buffer[i]);
    }

    arr[this->size() - 1] = value;
}

unsigned int Vector::size() {
    return this->current_size;
}

void Vector::clear() {
    delete arr;
    this->current_size = 0;
    arr = new std::string[current_size];
}

std::ostream& operator<<(std::ostream &os, Vector &vector) {
    os << "{ ";
    for (unsigned int i = 0; i < vector.size(); i++)
        os << vector.get(i) << (i + 1 == vector.size() ? "" : ", ");
    os << " }";
    return os;
}
