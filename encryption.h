#ifndef ENCRYPTION_H
#define ENCRYPTION_H

#include <string>
#include "vector.h"

class Encryption {
protected:
    
    Vector encrypted;
    
    Vector decrypted;
    
    unsigned int lines;
public:
    
    virtual void encrypt_content() = 0;
    
    virtual void decrypt_content() = 0;

   
    void read_encrypted_file(std::string in_path);
    
    void read_decrypted_file(std::string in_path);

    
    void write_encrypted_content(std::string out_path);
    
    void write_decrypted_content(std::string out_path);

    
    Vector get_encrypted_content();
    
    Vector get_decrypted_content();

    
    unsigned int lines_amount();

    
    void clear();

    virtual ~Encryption();
};

